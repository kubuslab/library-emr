<?php
namespace CoreEmr;

/**
 * Pemeriksaan
 */
abstract class Pemeriksaan extends Dokumen
{
    /** @var PerawatanMedis */
    public $perawatan;

    /** @var CoreEmr\Storage\OrangInterface|CoreEmr\Storage\PasienInterface|CoreEmr\Storage\RekamMedisInterface|CoreEmr\Storage\KasusMedisInterface|CoreEmr\Storage\PerawatanMedisInterface|CoreEmr\Storage\RujukanInterface|CoreEmr\Storage\PemeriksaanInterface */
    public $storage;

    /** @var CoreEmr\Info\InfoPemeriksaan */
    public $info;

    function __construct(PerawatanMedis $perawatan)
    {
        $this->perawatan = $perawatan;
        $this->storage = $perawatan->storage;
    }
}