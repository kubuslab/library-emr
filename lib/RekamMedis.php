<?php
namespace CoreEmr;

/**
 * Rekam Medis
 */
class RekamMedis extends Dokumen
{
	/** @var LoaderClass */
	private $loader;

	/** @var CoreEmr\Storage\OrangInterface|CoreEmr\Storage\PasienInterface|CoreEmr\Storage\RekamMedisInterface|CoreEmr\Storage\KasusMedisInterface|CoreEmr\Storage\PerawatanMedisInterface|CoreEmr\Storage\RujukanInterface|PemeriksaanInterface */
	public $storage;

	/** @var Pasien */
	public $pasien;

	/** @var array<string, KasusMedis> */
	public $kasus;

	/** @var CoreEmr\Info\InfoRekamMedis */
	public $info;

	function __construct()
	{
		// code...
	}

	/**
	 * [setLoader description]
	 * @param CoreEmr\LoaderClass $loader loader untuk mempermudah memuat plugin
	 * @return CoreEmr\RekamMedis
	 */
	public function setLoader($loader)
	{
		$this->loader = $loader;
		return $this;
	}

	/**
	 * 
	 * @param CoreEmr\Storage\OrangInterface|CoreEmr\Storage\PasienInterface|CoreEmr\Storage\RekamMedisInterface|CoreEmr\Storage\KasusMedisInterface|CoreEmr\Storage\PerawatanMedisInterface|PemeriksaanInterface $storage plugin storage untuk menyesuaikan dengan framework yang digunakan
	 * @return CoreEmr\RekamMedis
	 */
	public function setStorage($storage)
	{
		$this->storage = $storage;
		return $this;
	}
}