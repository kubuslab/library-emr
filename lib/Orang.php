<?php
namespace CoreEmr;

/**
 * Orang
 */
class Orang extends Dokumen
{

	/** @var Alamat */
	public $alamatSekarang;

	/** @var Alamat */
	public $alamatSebelumnya;

	/** @var Alamat */
	public $alamatKtp;

	/** @var array<string,Orang> Daftar kontak baik suami, ibu, bapak, anak, saudara, keluarga, teman dll */
	public $kontak = [];

	function __construct()
	{
		// code...
	}

	public function getKontak($tipe)
	{
		return isset($this->kontak[$tipe]) ? $this->kontak[$tipe] : null;
	}
}