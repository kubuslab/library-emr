<?php
namespace CoreEmr;

/**
 * Rujukan
 */
abstract class Rujukan extends Dokumen
{
	/** @var KasusMedis */
	public $kasusMedis;

	/** @var CoreEmr\Storage\OrangInterface|CoreEmr\Storage\PasienInterface|CoreEmr\Storage\RekamMedisInterface|CoreEmr\Storage\KasusMedisInterface|CoreEmr\Storage\PerawatanMedisInterface|CoreEmr\Storage\RujukanInterface|CoreEmr\Storage\PemeriksaanInterface */
	public $storage;

	/** @var array<string, Pemeriksaan> */
	public $pemeriksaan;
	
	/** @var CoreEmr\Info\InfoRujukan */
	public $info;

	function __construct(KasusMedis $kasusMedis)
	{
		$this->kasusMedis = $kasusMedis;
		$this->storage = $kasusMedis->storage;
	}
}