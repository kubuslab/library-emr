<?php
namespace CoreEmr;

/**
 * KasusMedis
 */
abstract class KasusMedis extends Dokumen
{
	/** @var RekamMedis */
	public $rekamMedis;

	/** @var CoreEmr\Storage\OrangInterface|CoreEmr\Storage\PasienInterface|CoreEmr\Storage\RekamMedisInterface|CoreEmr\Storage\KasusMedisInterface|CoreEmr\Storage\PerawatanMedisInterface|CoreEmr\Storage\RujukanInterface|CoreEmr\Storage\PemeriksaanInterface */
	public $storage;

	/** @var array<string, PerawatanMedis> */
	protected $perawatan;

	/** @var array<string, Rujkan> */
	protected $rujukan;	

	/** @var CoreEmr\Info\InfoKasusMedis */
	public $info;

	function __construct(RekamMedis $rekamMedis)
	{
		$this->rekamMedis = $rekamMedis;
		$this->storage = $rekamMedis->storage;
	}

	abstract public function adalahMultiSesi();

	public function getPerawatan() {
		return count($this->perawatan) > 0 ? $this->perawatan[0] : null;
	}

	public function getPerawatanArray() {
		return $this->perawatan;
	}
}