<?php

/**
 * PemeriksaanHandler
 */
abstract class PemeriksaanHandler
{
	protected $field;
    protected $module;

    /** @var Pemeriksaan */
    protected $pemeriksaan;

    function __construct($field, $pemeriksaan, $module)
    {
        $this->field = $field;
        $this->pemeriksaan = $pemeriksaan;
        $this->module = $module;

        // setup awal
        $this->setup();
    }

    public function getField()
    {
        return $this->field;
    }

    protected function setup() {/* PSEUDO ABSTRACT */}
}