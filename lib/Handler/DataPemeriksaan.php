<?php

/**
 * DataPemeriksaan
 */
abstract class DataPemeriksaan
{
	private static $DEFAULT_PLUGIN = array('vitalsign', 'fisikumum', 'lab', 'usg');

	/** @var CI_DB_query_builder */
	// protected $db;

	protected $nama;
	protected $modul;
	/** @var Ibuhamil */
	protected $ibuhamil;
	protected $id;
	protected $_pemeriksaan;
	protected $_pemeriksaanjanin;
	protected $_summary;
	protected $_summaryjanin;

	public function __construct($nama, $ibuhamil, $modul)
	{
		$this->nama = $nama;
		$this->ibuhamil = $ibuhamil;
		$this->modul = $modul;
	}

	abstract function info($muatUlang = false, $perawatan = null);
	abstract function summaryTerakhir($muatUlang = false, $perawatan = null);
	abstract function detail($kunjungan);

	public function nama() { return $this->nama; }
	public function namaSummary() { return $this->nama; }
	public function modul() { return $this->modul; }
	public function getPemeriksaan() { return $this->_pemeriksaan; }
	public function getPemeriksaanJanin() { return $this->_pemeriksaanjanin; }
	public function getSummary() { return $this->_summary; }
	public function getSummaryJanin() { return $this->_summaryjanin; }
	protected function metafields() { return null; }

	protected function konversiBilangan($object, $key, $null = true)
	{
		return $this->ibuhamil->konversiBilangan($object, $key, $null, $this->metafields());
	}

	protected function pemeriksaan($perawatan = INFO_PERAWATAN_ANC)
	{
		if ($perawatan == INFO_PERAWATAN_ANC)
		{
			$pemeriksaan = $this->ibuhamil->get('anc');
			if (empty($pemeriksaan))
				throw new AncException('Data pemeriksaan ANC belum dimuat', 8003);
		}
		elseif ($perawatan == INFO_PERAWATAN_MKB)
		{
			$mkb = $this->ibuhamil->get('mkb');
			if (empty($mkb))
				throw new AncException('Data pemeriksaan MKB belum dimuat', 8003);
			$pemeriksaan = array();
			foreach ($mkb as $m)
			{
				foreach ($m->pemeriksaan as $k2 => $p)
				{
					$pemeriksaan[$k2] = $p;
				}
			}
		}
		elseif ($perawatan == INFO_PERAWATAN_NIFAS)
		{
			$nifas = $this->ibuhamil->get('nifas');
			if (empty($nifas))
				throw new AncException('Data pemeriksaan NIFAS belum dimuat', 8003);
			$pemeriksaan = $nifas->pemeriksaan;
		}
		elseif ($perawatan == INFO_PERAWATAN_RUJUKAN)
		{
			$rujukan = $this->ibuhamil->get('rujukan');
			if (empty($rujukan))
				throw new AncException('Data pemeriksaan RUJUKAN belum dimuat', 8003);
			$pemeriksaan = array();
			foreach ($rujukan as $m)
			{
				foreach ($m->pemeriksaan as $k2 => $p)
				{
					$pemeriksaan[$k2] = $p;
				}
			}
		}
		elseif ($perawatan == INFO_PERAWATAN_WUS)
		{
			$kb = $this->ibuhamil->get('kb');
			if (empty($kb))
				throw new AncException('Data pemeriksaan KB belum dimuat', 8003);
			$pemeriksaan = array();
			foreach ($kb as $m)
			{
				foreach ($m->pemeriksaan as $k2 => $p)
				{
					$pemeriksaan[$k2] = $p;
				}
			}
		}

		return $pemeriksaan;
	}

	protected function summary($perawatan = INFO_PERAWATAN_ANC)
	{
		if ($perawatan == INFO_PERAWATAN_ANC)
		{
			$summary = $this->ibuhamil->get('ancSummary');
			if (!isset($summary) || empty($summary))
				throw new AncException('Data Kunjungan ANC harus dimuat terlebih dahulu', 8003);
		}
		elseif ($perawatan == INFO_PERAWATAN_MKB)
		{
			$summary = $this->ibuhamil->get('mkbSummary');
			if (!isset($summary) || empty($summary))
				throw new AncException('Data Pemeriksaan MKB harus dimuat terlebih dahulu', 8003);
		}
		elseif ($perawatan == INFO_PERAWATAN_NIFAS)
		{
			$summary = $this->ibuhamil->get('nifasSummary');
			if (!isset($summary) || empty($summary))
				throw new AncException('Data Pemeriksaan Nifas harus dimuat terlebih dahulu', 8003);
		}
		elseif ($perawatan == INFO_PERAWATAN_RUJUKAN)
		{
			$summary = $this->ibuhamil->get('rujukanSummary');
			if (!isset($summary) || empty($summary))
				throw new AncException('Data Pemeriksaan RUjukan harus dimuat terlebih dahulu', 8003);
		}
		elseif ($perawatan == INFO_PERAWATAN_WUS)
		{
			$summary = $this->ibuhamil->get('kbSummary');
			if (!isset($summary) || empty($summary))
				throw new AncException('Data Pemeriksaan KB harus dimuat terlebih dahulu', 8003);
		}
		else
			throw new Exception('Jenis perawatan tidak dikenali', 8003);

		return $summary;
	}
}