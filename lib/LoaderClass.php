<?php
namespace CoreEmr;

/**
 * Loader untuk class
 */
abstract class LoaderClass
{
	abstract function library($nama, $modul = null);
}