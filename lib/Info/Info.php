<?php
namespace CoreEmr\Info;

/**
 * Info
 */
abstract class Info
{
	public $id;
	public $dibuat;
	public $diubah;
	public $registrar;

	public function toArray()
	{
		return [
			'id' => $this->id,
			'dibuat' => $this->dibuat,
			'diubah' => $this->diubah,
			'registrar' => $this->registrar
		];
	}

	public function toObject()
	{
		return (object) $this->toArray();
	}
}