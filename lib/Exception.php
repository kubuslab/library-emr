<?php
namespace CoreEmr;

use Exception;
use Throwable;

/**
 * MedisException
 */
class MedisException extends Exception
{
	
	function __construct($message = '', $code = 0, Throwable $previous = null)
	{
		parent::__construct($message, $code, $previous);
	}
}